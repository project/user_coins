<?php

/**
 * @file
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\user_coins\Entity\TransactionInterface;

/**
 * Implements hook_theme().
 */
function user_coins_theme($existing, $type, $theme, $path) {
  return [
    'user_coins_balance_block' => [
      'variables' => [
        'balance' => NULL,
      ],
    ],
  ];
}

/**
 * The user balance is calculated and updated upon each transaction.
 *
 * Implements hook_entity_insert().
 */
function user_coins_entity_insert(EntityInterface $entity) {
  // Update user balance after new transaction is stored.
  if ($entity instanceof TransactionInterface) {
    user_coins_update_balance($entity);
  }
}

/**
 * A transaction is normally never updated but we add this just in case.
 *
 * Implements hook_entity_update().
 */
function user_coins_entity_update(EntityInterface $entity) {
  // Update user balance.
  if ($entity instanceof TransactionInterface) {
    user_coins_update_balance($entity);
  }
}

/**
 * A transaction is never deleted but we add this just in case.
 *
 * Implements hook_entity_delete().
 */
function user_coins_entity_delete(EntityInterface $entity) {
  // Update user balance.
  if ($entity instanceof TransactionInterface) {
    user_coins_update_balance($entity);
  }
}

/**
 * Function that update balance based on new stored transaction.
 */
function user_coins_update_balance(TransactionInterface $transaction) {
  $coins_manager = \Drupal::service('user_coins.coins_manager');
  $coins_manager->updateBalance($transaction->getOwnerId());
}
