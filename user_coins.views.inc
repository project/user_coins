<?php

/**
 * @file
 */

/**
 * Implements hook_views_data().
 */
function user_coins_views_data() {
  $data = [];
  // Base data.
  $data['user_coins_balance']['table']['group'] = t('User Coins Balance');
  $data['user_coins_balance']['table']['provider'] = 'user_coins';
  $data['user_coins_balance']['table']['base'] = [
    'field' => 'uid',
    'title' => t('User Coins Balance'),
    'help' => t('User Coins Balance data.'),
    'query_id' => 'views_query',
  ];
  $data['user_coins_balance']['balance'] = [
    'title' => t('User Coins Balance'),
    'help' => t('User Coins Balance.'),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
  ];
  $data['user_coins_balance']['uid'] = [
    'title' => t('User Coins UID'),
    'help' => t('User Coins UID'),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'relationship' => [
      'base' => 'users_field_data',
      'base field' => 'uid',
      'id' => 'standard',
      'label' => t('Users data'),
    ],
  ];
  return $data;
}
