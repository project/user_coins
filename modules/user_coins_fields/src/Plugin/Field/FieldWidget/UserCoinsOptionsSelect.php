<?php

namespace Drupal\user_coins_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;

/**
 * Plugin implementation of the 'options_select' widget.
 *
 * @FieldWidget(
 *   id = "user_coins_options_select",
 *   label = @Translation("Select list"),
 *   field_types = {
 *     "user_coins_spend",
 *     "user_coins_earn",
 *   },
 *   multiple_values = TRUE
 * )
 */
class UserCoinsOptionsSelect extends OptionsSelectWidget {

}
