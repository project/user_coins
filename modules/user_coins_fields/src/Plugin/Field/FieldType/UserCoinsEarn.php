<?php

namespace Drupal\user_coins_fields\Plugin\Field\FieldType;

use Drupal\options\Plugin\Field\FieldType\ListIntegerItem;

/**
 * Plugin implementation of the 'user_coins_earn' field type.
 *
 * @FieldType(
 *   id = "user_coins_earn",
 *   label = @Translation("User Coins Earn List"),
 *   description = @Translation("This field stores integer values from a list of allowed 'value => label' pairs, i.e. 'Lifetime in days': 1 => 1 day, 7 => 1 week, 31 => 1 month."),
 *   category = @Translation("User Coins"),
 *   default_widget = "user_coins_options_select",
 *   default_formatter = "user_coins_list_default",
 * )
 */
class UserCoinsEarn extends ListIntegerItem {

}
