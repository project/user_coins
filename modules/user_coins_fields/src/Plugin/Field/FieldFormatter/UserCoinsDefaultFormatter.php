<?php

namespace Drupal\user_coins_fields\Plugin\Field\FieldFormatter;

use Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter;

/**
 * Plugin implementation of the 'list_default' formatter.
 *
 * @FieldFormatter(
 *   id = "user_coins_list_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "user_coins_spend",
 *     "user_coins_earn",
 *   }
 * )
 */
class UserCoinsDefaultFormatter extends OptionsDefaultFormatter {

}
