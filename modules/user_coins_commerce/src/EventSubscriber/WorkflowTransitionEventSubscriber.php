<?php

namespace Drupal\user_coins_commerce\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\user_coins\CoinsManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 *
 */
class WorkflowTransitionEventSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The coins manager service.
   *
   * @var \Drupal\user_coins\CoinsManagerInterface
   */
  protected $coinsManager;

  /**
   * Constructs a new WorkflowTransitionEventSubscriber object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\user_coins\CoinsManagerInterface $coins_manager
   *   The coins manager service.
   */
  public function __construct(MessengerInterface $messenger, CoinsManagerInterface $coins_manager) {
    $this->messenger = $messenger;
    $this->coinsManager = $coins_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'state_machine.post_transition' => 'onPostTransition',
    ];
    return $events;
  }

  /**
   * Reacts to the 'state_machine.post_transition' event.
   *
   * When an order is completed and containes purchased user coins products
   * we add the coins to the user.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onPostTransition(WorkflowTransitionEvent $event) {
    if ($event->getTransition()->getToState()->getId() == 'completed'
    && $event->getEntity()->getEntityTypeId() == 'commerce_order'
    && $order = $event->getEntity()
    ) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
      $total_coins = 0;

      foreach ($order->getItems() as $order_item) {
        /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
        /**
         * @var \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation
         */
        $product_variation = $order_item->getPurchasedEntity();
        /** @var \Drupal\commerce_product\Entity\ProductAttributeValue $nb_coins_attribute */
        if (
          $product_variation->hasField('attribute_number_of_coins')
        && $product_variation->bundle() == 'coins_pack_variation'
        ) {
          $nb_coins_attribute = $product_variation->getAttributeValue('attribute_number_of_coins');
          if ($nb_coins_attribute->hasField('field_number_of_coins')) {
            $nb_coins_value = $nb_coins_attribute->field_number_of_coins->value;
            if ($nb_coins_value > 0) {
              $total_coins += $nb_coins_value * $order_item->getQuantity();
            }
          }
        }
      }

      if ($total_coins > 0) {
        $this->coinsManager->addCoinsTransaction(
          $order->getCustomer()->id(),
          'earn',
          $total_coins
        );
      }
    }
  }

}
