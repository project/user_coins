# User Coins

The user coins module by itself implements a virtual coins system associated to users.

- Users gets a coins balance
- Administrators can add/remove coins to users manually
- Users can earn/spend their coins based on users coins transactions based on entity api (Developers can easily write a custom module that leverage on the entity api in order to implements custom logic that triggers earning/spending coins)
- With the help of the "user coins fields" sub module, user coins fields can be attached to nodes and makes it possible to earn or spend coins when creating a node.
- With the help of the "user coins commerce" sub module, it is possible to buy coins using Drupal commerce.

Go to the drupal module page: https://www.drupal.org/project/user_coins

## How to use

### User Coins base module

1) Install the user_coins module
2) Configure the permissions for the coins transactions (who can administer, who can create transactions, who can use/earn/spend the coins)
3) Optional: Place the user coins balance block in your region of choice for your theme at /admin/structure/block
4) Go to /admin/content/user_coins_transactions as a user who has the permissions to administer/create transactions
5) Try to add your first user coins transaction. The first time, you can only use a "earn" type transaction since the balance is 0 coins and no one owns coins yet.
6) Check the user balance at /admin/content/user_coins_balances

### User Coins Fields base module

1) Install the user_coins_fields module
2) Add a user coin fields inside one node content type
3) In allowed values, fill each number of coins with a label. For example:
5|earn 5 coins
10|earn 10 coins
4) Try to create a node with a user who has the permission to earn/spend coins
5) Check transactions and balances at /admin/content/user_coins_transactions and /admin/content/user_coins_balances

### User Coins Commerce module

1) Install Drupal commerce, product, order, payment, cart, checkout. (For inline entity form and if using composer,
type `composer require drupal/inline_entity_form @RC` before `composer require drupal/commerce` to solve some dependencies
issues)
2) Configure at least one store in Drupal Commerce
3) Install user_coins_commerce module
4) Go to /admin/commerce/product-attributes, for the attribute "Number of coins" add values to name/field_number_of_coins
5) Go to /product/add/coins_pack and add a coins pack product by filling the title and using the button "save and add variations"
6) Now add as many variations as you like for your coins pack.
7) Configure and setup payment, cart, etc
8) With a user with the right permissions, go to product page, add a variation to cart and checkout order
9) At order completion, the coins will be added to your coins balance
10) Check coins transactions and coins balances at /admin/content/user_coins_transactions and /admin/content/user_coins_balances

## Credits

This module is developed and maintained by [Koriolis](https://koriolis.fr/)

