<?php

namespace Drupal\user_coins;

/**
 * User Coins manager interface.
 */
interface CoinsManagerInterface {

  /**
   * Adds a coins transaction.
   *
   * @param int $uid
   *   The uid for the transaction.
   * @param string $type
   *   The type for the transaction (earn or spend).
   * @param int $amount
   *   The amount of coins for the transaction.
   */
  public function addCoinsTransaction(int $uid, string $type, int $amount);

  /**
   * Updates the user coins balance for a user.
   *
   * @param int $uid
   *   The uid for the transaction.
   */
  public function updateBalance(int $uid);

  /**
   * Load the user coins balance for a user.
   *
   * @param int $uid
   *   The uid for the transaction.
   *
   * @return int
   *   The user balance.
   */
  public function loadBalance(int $uid);

  /**
   * Calculate the user coins balance for a user.
   *
   * @param int $uid
   *   The uid for the transaction.
   *
   * @return int
   *   The user balance.
   */
  public function calculateBalance(int $uid);

}
