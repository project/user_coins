<?php

namespace Drupal\user_coins;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Provides a route access control handler for the views transactions route.
 */
class ViewsRouteTransactionsAccessControlHandler {

  /**
   *
   */
  public function access(AccountInterface $account, $user) {
    if ($account->hasPermission('administer user coins transactions')) {
      return AccessResult::allowed();
    }
    elseif ($account->id() == $user && $account->hasPermission('use user coins')) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }
}
