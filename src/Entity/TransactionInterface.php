<?php

namespace Drupal\user_coins\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for Transaction entity.
 *
 * @ingroup user_coins
 */
interface TransactionInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Get the transaction timestamp.
   *
   * @return int
   *   The transaction timestamp.
   */
  public function getCreatedTime();

  /**
   * Set the created time.
   *
   * @param int $timestamp
   *   The timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

  /**
   * Get the transaction amount.
   *
   * @return int
   *   The transaction amount.
   */
  public function getAmount();

  /**
   * Set the brut amount.
   *
   * @param int $amount
   *   The transaction amount.
   *
   * @return $this
   */
  public function setAmount($amount);

  /**
   * Get the transaction status.
   *
   * @return int
   *   The transaction status.
   */
  public function getStatus();

  /**
   * Set the status.
   *
   * @param int $status
   *   The transaction status.
   *
   * @return $this
   */
  public function setStatus($status);

  /**
   * Get the transaction hash.
   *
   * @return string
   *   The transaction hash.
   */
  public function getHash();

  /**
   * Set the hash.
   *
   * @param string $hash
   *   The transaction hash.
   *
   * @return $this
   */
  public function setHash($hash);

  /**
   * Get the transaction type.
   *
   * @return string
   *   The transaction type.
   */
  public function getType();

  /**
   * Set the transaction type.
   *
   * @param string $type
   *   The type.
   *
   * @return $this
   */
  public function setType($type);

}
