<?php

namespace Drupal\user_coins\Entity;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Provides transaction entity.
 *
 * @ingroup user_coins
 *
 * @ContentEntityType(
 *   id = "user_coins_transaction",
 *   label = @Translation("Transaction"),
 *   label_collection = @Translation("Transactions"),
 *   label_singular = @Translation("transaction"),
 *   label_plural = @Translation("transactions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count transaction",
 *     plural = "@count transactions",
 *   ),
 *   bundle_label = @Translation("Transaction type"),
 *   handlers = {
 *     "storage_schema" = "Drupal\user_coins\TransactionStorageSchema",
 *     "access" = "Drupal\user_coins\TransactionAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\user_coins\Form\TransactionForm",
 *     },
 *   },
 *   base_table = "user_coins_transactions",
 *   admin_permission = "administer transactions",
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "transaction_id",
 *     "label" = "amount",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "uid" = "uid",
 *   },
 *   links = {
 *     "add-form" = "/user_coins_transaction/add",
 *   },
 *   fieldable = FALSE,
 * )
 */
class Transaction extends ContentEntityBase implements TransactionInterface {

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->getEntityKey('owner')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('owner', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->getEntityKey('owner');
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('owner', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAmount() {
    return $this->get('amount')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAmount($amount) {
    $this->set('amount', $amount);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->get('type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setType($type) {
    $this->set('type', $type);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHash() {
    return $this->get('hash')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHash($hash) {
    $this->set('hash', $hash);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Uid'))
      ->setDescription(t('The uid of the transaction.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
      ])
      ->setSetting('target_type', 'user')
      ->addConstraint('TargetUserAllowedToUseUserCoins');

    $fields['type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Transaction type'))
      ->setDescription(t('Transaction type.'))
      ->setSettings([
        'allowed_values' => [
          'earn' => t('Earn coins'),
          'spend' => t('Spend coins'),
        ],
      ])
      ->setDefaultValue('earn')
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDefaultValue('');

    $fields['amount'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Amount'))
      ->setDescription(t('The amount of the transaction.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'number',
      ])
      ->setSetting('display_description', TRUE)
      ->addConstraint('AmountSpentBellowBalance');

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDescription(t('The status of the transaction.'))
      ->setRequired(TRUE)
      ->setDefaultValue(1)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the transaction has been created.'))
      ->setTranslatable(TRUE);

    $fields['hash'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Hash'))
      ->setDescription(t('Unique transaction hash.'))
      ->setDefaultValueCallback('Drupal\user_coins\Entity\Transaction::hashGenerate')
      ->setRequired(TRUE)
      ->setDefaultValue('');

    return $fields;
  }

  /**
   * Generate a unique URL-safe hash.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return string
   *   A unique up to 4 * 12 character identifier.
   */
  public static function hashGenerate() {

    $hash = Crypt::randomBytesBase64(12);
    // Make sure hash is unique.
    if (\Drupal::database()->query("SELECT transaction_id FROM {user_coins_transactions} WHERE hash = :hash", [':hash' => $hash])->fetchObject()) {
      $hash = self::hashGenerate();
    }

    return $hash;
  }

}
