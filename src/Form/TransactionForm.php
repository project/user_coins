<?php

namespace Drupal\user_coins\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\user_coins\Entity\TransactionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the user_coins entity edit forms.
 */
class TransactionForm extends ContentEntityForm {

  /**
   * The route provider service.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, RouteProviderInterface $route_provider) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->routeProvider = $route_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('router.route_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();

    if ($result == SAVED_NEW && $entity instanceof TransactionInterface) {
      $message = 'Transaction type : ' . $entity->getType() . ', uid: ' . $entity->getOwnerId() . ', amount: ' . $entity->getAmount() . ' saved.';
      $this->messenger()->addStatus($message);
      $this->logger('user_coins')->notice($message);
      if (count($this->routeProvider->getRoutesByNames(['view.user_coins_transactions.page_2'])) === 1) {
        $form_state->setRedirect('view.user_coins_transactions.page_2');
      }
      else {
        $form_state->setRedirect('user_coins_transaction.add');
      }
    }
  }

}
