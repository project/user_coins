<?php

namespace Drupal\user_coins;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a Transaction access control handler.
 */
class TransactionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\user_coins\TransactionInterface $entity */
    if ($operation == 'view') {
      if ($account->hasPermission('administer user coins transactions')) {
        return AccessResult::allowed();
      }
      elseif ($account->id() == $entity->getOwnerId() && $account->hasPermission('use user coins')) {
        return AccessResult::allowed();
      }
    }

    if ($operation == 'delete') {
      return AccessResult::forbidden('Transaction cannot not be deleted.');
    }

    // No opinion.
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    if ($account->hasPermission('create user coins transactions')) {
      return AccessResult::allowed();
    }

    // No opinion.
    return AccessResult::forbidden();
  }

}
