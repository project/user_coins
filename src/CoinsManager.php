<?php

namespace Drupal\user_coins;

use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\user_coins\Entity\Transaction;

/**
 * User Coins transactions and balance manager.
 */
class CoinsManager implements CoinsManagerInterface {
  /**
   * The db connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Class constructor.
   */
  public function __construct(Connection $connection, LoggerChannelFactoryInterface $logger_factory) {
    $this->connection = $connection;
    $this->logger = $logger_factory;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function addCoinsTransaction(int $uid, string $type, int $amount) {
    $transaction = Transaction::create([
      'uid' => $uid,
      'type' => $type,
      'status' => 1,
      'amount' => $amount,
      'created' => time(),
    ]);

    if (($violations = $transaction->validate()) && $violations->count()) {
      $validation_errors = [];
      foreach ($violations as $violation) {
        $validation_errors[] = $transaction->id() . ': ' . $violation->getMessage();
      }
      $messages = implode(', ', $validation_errors);
      $this->logger->get('user_coins')->error($messages);
      throw new \Exception($messages);
    }
    else {
      $transaction->save();
      $message = 'Transaction type : ' . $type . ', uid: ' . $uid . ', amount: ' . $amount . ' saved.';
      $this->logger->get('user_coins')->notice($message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateBalance($uid) {
    $balance = $this->calculateBalance($uid);

    // Insert or update the balance.
    $this->connection
      ->merge('user_coins_balance')
      ->fields([
        'balance' => $balance,
      ])
      ->key('uid', $uid)
      ->execute();

    $message = 'Balance amount: ' . $balance . ' for uid: ' . $uid . ' updated.';
    $this->logger->get('user_coins')->notice($message);
  }

  /**
   * {@inheritdoc}
   */
  public function loadBalance($uid) {
    $balance = $this->connection
      ->query(
        "SELECT balance FROM {user_coins_balance} where uid=:uid",
        [
          ':uid' => $uid,
        ]
      )
      ->fetchField();
    if (!$balance) {
      $balance = 0;
    }
    return $balance;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateBalance($uid) {
    // Static query to get the balance.
    $balance = $this->connection
      ->query("
        SELECT
        (SELECT IFNULL(SUM(amount), 0)
        FROM {user_coins_transactions}
        WHERE type=:earn
        AND status=:status
        AND uid=:uid)
        -
        (SELECT IFNULL(SUM(amount),0)
        FROM {user_coins_transactions}
        WHERE type=:spend
        AND status=:status
        AND uid=:uid)
        ",
        [
          ':earn' => 'earn',
          ':spend' => 'spend',
          ':status' => 1,
          ':uid' => $uid,
        ]
      )
      ->fetchField();

    if (!$balance) {
      $balance = 0;
    }

    return $balance;
  }

}
